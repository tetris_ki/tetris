package ActivationFunctions;

public class ReLU implements ActivationFunction {
    @Override
    public float activation(float input) {
        return input >= 0 ? input : 0;
    }

    @Override
    public float derivative(float input) {
        return input >= 0 ? 1 : 0;
    }
}
