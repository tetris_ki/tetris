package AI;

import ActivationFunctions.ActivationFunction;
import Tetrisgame.Main;

import java.security.spec.RSAOtherPrimeInfo;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

public class NeuralNetwork {
    int numberChanges = 0;
    float phi = 1; // ExplorationRate
    float gamma = 0.9f; // Rate of decrease
    Layer[] layers;
    ArrayList<Experience> experiences = new ArrayList<>();

    public static NeuralNetwork networkPolicy;
    public static NeuralNetwork networkOptimalQFunction;

    public static final float EPSILON = 0.01f;
    public static final int EXPERIENCE_BATCH_SIZE = 256;
    public static final int EXPERIENCE_CAPACITY = 100000;
    public static final int NUM_ACTIONS = Main.X_FIELDS * 4;

    public static boolean exploring = true;

    public NeuralNetwork(Layer[] layers) {
        this.layers = layers;
        initConnections();
        this.layers[this.layers.length - 1].isOutputLayer = true;
    }

    public void initConnections() {
        int i = 0;
        for (Layer layer : layers) {
            if (i == layers.length - 1) {
                break;
            }
            ArrayList<Neuron> neuronsNextLayer = layers[i + 1].neurons;
            for (Neuron neuron : layer.neurons) {
                for (Neuron nextNeuron : neuronsNextLayer) {
                    Connection connection = new Connection(neuron, nextNeuron);
                    neuron.outConnections.add(connection);
                    nextNeuron.inConnections.add(connection);
                }
            }
            i++;
        }
    }

    float lossSum;

    public void updateNetworkWithExperiences() {
        float loss = 0;
        for (int counter = 0; counter < EXPERIENCE_BATCH_SIZE; counter++) {
            Experience oldExperience = experiences.get(new Random().nextInt(experiences.size()));

            //networkPolicy.calculateOutput(oldExperience.stateTPlus1);
            float qStar = (float) oldExperience.rewardTPlus1; //+ gamma * networkPolicy.layers[layers.length - 1].neurons.get(oldExperience.action).value;

            networkPolicy.calculateOutput(oldExperience.stateT);
            float q = networkPolicy.layers[layers.length - 1].neurons.get(oldExperience.action).value;

            loss = qStar - q;
            lossSum += loss;
            //System.out.println("LOL" + loss);
            networkPolicy.backprogate(loss, EPSILON, oldExperience.action);
        }
        if(numberChanges % 100 == 0){
            //networkOptimalQFunction = networkPolicy.copy();
        }
        if (numberChanges % 100 == 0) {

            System.out.println("changed: " + (lossSum / 100));
            System.out.println("reward: " + (Main.diffRewards / 100));

            lossSum = 0;
            Main.diffRewards = 0;

            //test(new float[]{0, 0, 0, 1, 0});



        }
    }

    public void test(float[] input){
        networkPolicy.calculateOutput(input);

        System.out.println("Left: " + networkPolicy.layers[networkPolicy.layers.length - 1].neurons.get(0).value);
        System.out.println("Right: " + networkPolicy.layers[networkPolicy.layers.length - 1].neurons.get(1).value);
    }

    public void updateGame(Main game, float[] input) {
        numberChanges++;
        Experience newExperience;
        if (new Random().nextFloat() < phi) {
            exploring = true;
            int randomAction = new Random().nextInt(NUM_ACTIONS);
            float reward = game.update(randomAction);
            float[] inputTPlus1 = game.convertForAI();
            newExperience = new Experience(input, reward, randomAction, inputTPlus1);

        } else {
            exploring = false;
            float[] inputs = game.convertForAI();
            networkPolicy.calculateOutput(inputs);
            int highestNumber = 0;
            for (int duration = 1; duration < layers[layers.length - 1].neurons.size(); duration++) {
                if (layers[layers.length - 1].neurons.get(duration).value > layers[layers.length - 1].neurons
                        .get(highestNumber).value) {
                    highestNumber = duration;
                }
            }
            game.update(highestNumber);
            game.convertForAI();
            float[] inputTPlus1 = game.convertForAI();
            newExperience = new Experience(input, game.getReward(), highestNumber, inputTPlus1);
        }
        if(experiences.size() >= EXPERIENCE_CAPACITY){
            experiences.remove(0);
        }
        experiences.add(newExperience);

        if (numberChanges % 10 == 0 && experiences.size() >= EXPERIENCE_BATCH_SIZE - 1) {
            updateNetworkWithExperiences();
        }

        phi -= 0.01;
        if(phi < 0.01) phi = 0.01f;
    }

    public void calculateOutput(float[] inputs) {
        //System.out.println(inputs.length + " : " + layers[0].neurons.size() + " : " + numberChanges + " : " + this.networkPolicy);
        if (inputs.length != layers[0].neurons.size()) {
            throw new IllegalArgumentException("Input Size entspricht nicht Input Layer Size");
        }

        int i = 0;
        for (Neuron neuron : layers[0].neurons) {
            neuron.value = inputs[i];
            i++;
        }

        for (Layer layer : layers) {
            layer.calculate();
        }
    }

    public NeuralNetwork copy() {
        NeuralNetwork network = new NeuralNetwork(new Layer[]{new Layer(Main.X_FIELDS * Main.Y_FIELDS, ActivationFunction.ActivationSigmoid),
                new Layer(200, ActivationFunction.ActivationSigmoid),
                new Layer(200, ActivationFunction.ActivationSigmoid),
                new Layer(Main.X_FIELDS * 4, ActivationFunction.ActivationIdentity)});
        int i = 0;
        for (Layer layer : this.layers) {
            int j = 0;
            network.layers[i] = new Layer(layer.neurons.size(), layer.neurons.get(0).function);
            for (Neuron neuron : layer.neurons) {
                neuron.inConnections = layer.neurons.get(j).inConnections;
                neuron.outConnections = layer.neurons.get(j).outConnections;
                j++;
            }
            i++;
        }
        return network;
    }

    public void backprogate(float loss, float epsilon, int indexAction) {
        for (Layer layer : layers) {
            for (Neuron neuron : layer.neurons) {
                neuron.smallDelta = 0;
            }
        }
        layers[layers.length-1].neurons.get(indexAction).smallDelta = loss;
        for (Layer layer : layers) {
            for (Neuron neuron : layer.neurons) {
                neuron.backpropagateSmallDelta();
            }
        }
        for (Layer layer : layers) {
            for (Neuron neuron : layer.neurons) {
                neuron.deltaLearning(epsilon);
            }
        }
    }

    public static void main(String[] args) {
        networkPolicy = new NeuralNetwork(
                new Layer[]{new Layer(Main.X_FIELDS * Main.Y_FIELDS, ActivationFunction.ActivationSigmoid),
                        new Layer(150, ActivationFunction.ActivationSigmoid),
                        new Layer(70, ActivationFunction.ActivationSigmoid),
                        new Layer(NUM_ACTIONS, ActivationFunction.ActivationSigmoid)});
        networkOptimalQFunction = networkPolicy.copy();
        float[] state = new float[Main.X_FIELDS * Main.Y_FIELDS];
        for (int i = 0; i < state.length * 100; i++) {
            Main game = new Main();
            game.startGame();
            while (game.isGameRunning()) {
                state = game.convertForAI();
                networkPolicy.updateGame(game, state);
            }
        }
        System.out.println("\n\n------------------- ERGEBNIS ---------------------\n\n");
    }


    /*
            networkPolicy = new NeuralNetwork(
                new Layer[]{new Layer(5, ActivationFunction.ActivationSigmoid),
                        new Layer(150, ActivationFunction.ActivationSigmoid),
                        //new Layer(50, ActivationFunction.ActivationSigmoid),
                        new Layer(NUM_ACTIONS, ActivationFunction.ActivationSigmoid)});
        networkOptimalQFunction = networkPolicy.copy();

        networkPolicy.experiences = new ArrayList<Experience>(Arrays.asList(
            new Experience(new float[]{1, 0, 0, 0, 0}, 0.5, 1, new float[]{0, 1, 0, 0, 0}),
            new Experience(new float[]{0, 0, 0, 1, 0}, 1, 0, new float[]{0, 0, 1, 0, 0}),
            new Experience(new float[]{1, 0, 0, 0, 0}, 0, 0, new float[]{1, 0, 0, 0, 0}),
            new Experience(new float[]{0, 0, 0, 0, 1}, 0.5, 0, new float[]{0, 0, 0, 1, 0}),
            new Experience(new float[]{0, 0, 0, 0, 1}, 0, 1, new float[]{0, 0, 0, 0, 1}),
            new Experience(new float[]{0, 0, 1, 0, 0}, 0.5, 1, new float[]{0, 0, 0, 1, 0}),
            new Experience(new float[]{0, 0, 1, 0, 0}, 0.5, 0, new float[]{0, 1, 0, 0, 0}),
            new Experience(new float[]{0, 1, 0, 0, 0}, 1, 1, new float[]{0, 0, 1, 0, 0}),
            new Experience(new float[]{0, 1, 0, 0, 0}, 1, 1, new float[]{0, 0, 1, 0, 0}),
            new Experience(new float[]{0, 1, 0, 0, 0}, 0, 0, new float[]{1, 0, 0, 0, 0}),
            new Experience(new float[]{1, 0, 0, 0, 0}, 0, 0, new float[]{1, 0, 0, 0, 0}),
            new Experience(new float[]{0, 0, 0, 1, 0}, 1, 0, new float[]{0, 0, 1, 0, 0}),
            new Experience(new float[]{0, 0, 0, 0, 1}, 0.5, 0, new float[]{0, 0, 0, 1, 0}),
            new Experience(new float[]{0, 0, 0, 1, 0}, 0, 1, new float[]{0, 0, 0, 0, 1}),
            new Experience(new float[]{0, 0, 0, 0, 1}, 0.5, 0, new float[]{0, 0, 0, 1, 0})
        ));

                while (networkPolicy.numberChanges < 0) {
            networkPolicy.updateNetworkWithExperiences();
            networkPolicy.numberChanges++;
        }
     */
}
