package AI;

import java.util.Random;

public class Connection {

	Connection(Neuron startNeuron, Neuron endNeuron){
		this.startNeuron = startNeuron;
		this.endNeuron = endNeuron;
		this.weight = new Random().nextFloat() * 2 - 1;
	}

	float weight;
	
	Neuron startNeuron;
	Neuron endNeuron;
}
