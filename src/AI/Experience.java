package AI;

public class Experience {
    float[] stateT;
    double rewardTPlus1;
    int action;
    float[] stateTPlus1;

    public Experience(float[] StateT, double RewardTPlus1, int Action, float[] StateTPlus1) {
        this.stateT = StateT;
        this.rewardTPlus1 = RewardTPlus1;
        this.action = Action;
        this.stateTPlus1 =StateTPlus1;
    }
}
