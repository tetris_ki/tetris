package AI;

import java.util.ArrayList;

import ActivationFunctions.ActivationFunction;

public class Neuron {
	ArrayList<Connection> inConnections = new ArrayList<Connection>();
	ArrayList<Connection> outConnections = new ArrayList<Connection>();
	ActivationFunction function;
	
	float bias = 0f;
	float value;
	float smallDelta;
	
	public Neuron(ActivationFunction function) {
		this.function = function;
	}

	public void calculate(){
		value = function.activation((float) value);

		for(Connection connection : outConnections){
			connection.endNeuron.value += value * connection.weight;
		}
	}

	public boolean isInputNeuron() {
		return  inConnections.isEmpty();
	}

	public boolean isOutputNeuron() {
		return  outConnections.isEmpty();
	}

	public void calculateOutputDelta(float should) {
		smallDelta = should - value;
	}

	public void backpropagateSmallDelta() {
		if(isInputNeuron()) return;

		for (Connection connection : inConnections) {
			Neuron neuron = connection.startNeuron;
			neuron.smallDelta += smallDelta * connection.weight;
		}
	}

	public void deltaLearning(float epsilon) {
		float bigDeltaFactor = function.derivative((float) value) * epsilon * smallDelta;

		for (Connection connection : inConnections) {

			float bigDelta =  bigDeltaFactor * connection.startNeuron.value;
			connection.weight += bigDelta;
		}
	}

}
