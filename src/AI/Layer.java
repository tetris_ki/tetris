package AI;

import java.util.ArrayList;

import ActivationFunctions.ActivationFunction;

public class Layer {
	public ArrayList<Neuron> neurons = new ArrayList<>();
	boolean isOutputLayer = false;

	public Layer(int size, ActivationFunction function) {
		for (int i = 0; i < size; i++) {
			neurons.add(new Neuron(function));
		}
	}

	public void calculate() {

		for (Neuron neuron : neurons) {
			if (!isOutputLayer)
				neuron.calculate();
			else {
				neuron.value = neuron.function.activation((float) neuron.value);
				//System.out.println(neuron.value);
			}
		}
	}
}
