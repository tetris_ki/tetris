package Tetrisgame;

import java.util.Random;

public class Block {

    //Variables
    public int[] PosX;
    public int[] PosY;

    //Static
    public static final int StartX = Main.X_FIELDS/2 - 2;
    public static final int StartY = 0;

    //Constants
    public final int[][][] BLOCKS = {
            { new int[]{StartX, StartX + 1, StartX + 2, StartX + 3}, new int[]{StartY, StartY, StartY, StartY} },
            { new int[] {StartX, StartX + 1, StartX + 2, StartX}, new int[] {StartY, StartY, StartY, StartY + 1} },
            { new int[] {StartX, StartX + 1, StartX + 2, StartX + 2}, new int[] {StartY, StartY, StartY, StartY + 1} },
            { new int[] {StartX, StartX + 1, StartX + 1, StartX + 2}, new int[] {StartY, StartY, StartY + 1, StartY + 1} },
            { new int[] {StartX, StartX + 1, StartX + 1, StartX + 2}, new int[] {StartY + 1, StartY + 1, StartY, StartY} },
            { new int[] {StartX + 1, StartX + 1, StartX + 2, StartX + 2}, new int[] {StartY + 1, StartY, StartY + 1, StartY} },
            { new int[] {StartX, StartX + 1, StartX + 2, StartX + 1}, new int[] {StartY, StartY, StartY, StartY + 1} }
    };

    //Constructor
    public Block() {
        int random = new Random().nextInt(7);
        this.PosX = BLOCKS[random][0];
        this.PosY = BLOCKS[random][1];
    }

    //Methods
    public void raisePosB(Block block, int Change){
    	for (int i = 0; i < PosX.length; i++)
			block.PosX[i] += Change;
    }

    public void raisePosZ(Block block) {
    	for (int i = 0; i < PosY.length; i++)
			block.PosY[i] += 1;
    }
    //changed
    public int checkPos(int Pos, int IndexOutOfArray, int Change) {
        if (Pos == IndexOutOfArray)
            return (IndexOutOfArray - Change);
        else return Pos;
    }
    public int checkPos2(int Pos, int IndexOutOfArray, int Change) {
        if (Pos >= IndexOutOfArray)
            return (IndexOutOfArray - Change);
        else return Pos;
    }
}

