package Tetrisgame;

import ActivationFunctions.ActivationFunction;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Main {

    // Constants
    public final static int FIELD_SIZE = 30;
    public final static int X_FIELDS = 12;
    public final static int Y_FIELDS = 25;
    public final static boolean FRAME_ON = true;

    // Variables and fields
    Rotation rotation = new Rotation(this);
    Frame frame;
    Block block;
    float points = 0;
    float reward = 0;
    float lastStateReward = 0;
    int RowRemoveCounter = 0;
    int[] MatrixRow = new int[8];
    int moveCounter = 0;
    boolean gameRunning = true;
    boolean[][] fieldTurning = new boolean[4][4];
    boolean[][] field = new boolean[Y_FIELDS][X_FIELDS];
    boolean[][] fieldOldBlocks = new boolean[Y_FIELDS][X_FIELDS];
    Movement mainMovement = new Movement(this);

    public float getReward() {
        return reward;
    }

    public boolean isGameRunning() {
        return gameRunning;
    }

    public void moveTopPartDown(int BlockPosX) {
        for (int YPos = BlockPosX - 1; YPos >= 0; YPos--) {
            for (int XPos = 0; XPos < X_FIELDS; XPos++) {
                if (fieldOldBlocks[YPos][XPos]) {
                    fieldOldBlocks[YPos + 1][XPos] = true;
                    fieldOldBlocks[YPos][XPos] = false;
                }
            }
        }
    }

    public Block newBlock() {
        Block myNextPiece = new Block();
        for (int i = 0; i < 4; i++) {
            if (fieldOldBlocks[myNextPiece.PosY[i]][myNextPiece.PosX[i]]) {
                gameRunning = false;
                if(frame != null) frame.dispose();
                //System.out.println("You lost the game! You managed to get " + points + " Points");
                break;
            }
        }
        return myNextPiece;
    }

    public void listToPos(Block block) {
        for (int i = 0; i < block.PosX.length; i++) {
            block.PosY[i] = MatrixRow[i * 2];
            block.PosX[i] = MatrixRow[i * 2 + 1];
        }
        //mainMovement.moveDown(block);
    }

    public void getPosFromArray(Block block) {
        int XChange = arraySorter(block.PosX, true);
        int MatrixRowCounter = 0;
        moveCounter++;
        // System.out.println(moveCounter);
        for (int PosXCheck = 0; PosXCheck < 4; PosXCheck++) {
            if (PosXCheck + XChange >= X_FIELDS) {
                XChange = XChange - 2;
            }
        }
        for (int PosY = 0; PosY < 4; PosY++) {
            for (int PosX = 0; PosX < 4; PosX++) {
                if (fieldTurning[PosY][PosX]) {
                    MatrixRow[MatrixRowCounter] = PosY; // + rotation.yChange - rotation.yMoved;
                    MatrixRowCounter++;
                    MatrixRow[MatrixRowCounter] = PosX + XChange;
                    MatrixRowCounter++;
                    fieldTurning[PosY][PosX] = false;
                }
            }
        }
    }

    public int arraySorter(int[] Pos, boolean smallNumber) {
        int a = Pos.length - 1;
        if (smallNumber) {
            a = 0;
        }
        int sortedArray[] = new int[Pos.length];
        for (int i = 0; i < Pos.length; i++) {
            sortedArray[i] = Pos[i];
        }
        Arrays.sort(sortedArray);
        return sortedArray[a];
    }

    public void startMatrix(Block block) {
        int yChange = arraySorter(block.PosY, true);
        int xChange = arraySorter(block.PosX, true);
        setBlock(block, false, field);

        for (int i = 0; i < block.PosY.length; i++)
            fieldTurning[block.PosY[i] - yChange][block.PosX[i] - xChange] = true;

        fieldTurning = rotation.rotate(fieldTurning, block);
        getPosFromArray(block);
        listToPos(block);
    }

    public static int diffRewards;

    public float calculateReward() {
    	float stateCounter = 0;
    	for (boolean[] row : fieldOldBlocks) {
    		float lineReward = 0;
    		 for (boolean state : row) {
    			 if (state) {
    				 lineReward++;
    			 }
    			 else {
    				 lineReward -= 1;
    			 }
    		 }if (lineReward == -X_FIELDS) {lineReward= 0;}
    		 stateCounter += lineReward;
    	}
    	float diffReward = stateCounter - lastStateReward;
    	lastStateReward = stateCounter;
    	diffRewards += diffReward;
    	//System.out.println("stateReward: " + stateCounter + "        diffReward: " + diffReward);
    	//return ActivationFunction.ActivationSigmoid.activation(diffReward);
        return  diffReward;
    }

    public void checkLineNowFR(int XPos) {
        boolean lineFull = true;
        for (int PosXTest = 0; PosXTest < X_FIELDS; PosXTest++) {
            if (!fieldOldBlocks[XPos][PosXTest]) {
                lineFull = false;
                break;
            }
        }
        if (lineFull) {
            for (int PosXChange = 0; PosXChange < X_FIELDS; PosXChange++) {
                fieldOldBlocks[XPos][PosXChange] = false;
            }
            moveTopPartDown(XPos);
            RowRemoveCounter++;
        }
    }

    public void checkLine(Block block) {
        RowRemoveCounter = 0;
        for (int pos : block.PosY) {
            checkLineNowFR(pos);
        }
        if (RowRemoveCounter > 0) {
            reward = (float) Math.pow(10.0, (double) RowRemoveCounter + 1);
            points = points + reward;
        } else {
            reward = 0;
        }
        reward += calculateReward();
    }

    public void confirmMove() {
        Movement mainMovement = new Movement(this);
        while (mainMovement.moveDown(block)) { }
        block = newBlock();
    }

    public void setBlock(Block block, boolean Gen, boolean[][] field) {
        for (int i = 0; i < block.PosY.length; i++)
            field[block.PosY[i]][block.PosX[i]] = Gen;
    }

    public float[] convertForAI() {
        float ValuesForAi[] = new float[X_FIELDS * Y_FIELDS];
        for (int PosY = 0; PosY < Y_FIELDS; PosY++) {
            for (int PosX = 0; PosX < X_FIELDS; PosX++) {
                if (fieldOldBlocks[PosY][PosX]) {
                    ValuesForAi[PosX + (PosY * X_FIELDS)] = 1.0f;
                } else if (field[PosY][PosX]) {
                    ValuesForAi[PosX + (PosY * X_FIELDS)] = -1f;
                } else {
                    ValuesForAi[PosX + (PosY * X_FIELDS)] = 0.0f;
                }
            }
        }
        return ValuesForAi;
    }

    public float update(int command) {
        if (gameRunning) {
            int x = command % X_FIELDS;
            int rotation = command / X_FIELDS;

            for (int i = 0; i < rotation; i++) {
                startMatrix(block);
            }
            boolean moving = true;
            int Counter = 0;
            while (moving) {
                if (block.PosX[0] > x) {
                    mainMovement.moveLeft(block);
                } else if (block.PosX[0] < x) {
                    mainMovement.moveRight(block);
                } else {
                    moving = false;
                }
                Counter++;
                if (Counter >= X_FIELDS){
                    moving = false;
                }
            }

            confirmMove();

            setBlock(block, true, field);
            if (FRAME_ON && frame != null)
                frame.repaint();
        } else {
            return 0;
        }
        return reward;
    }

    // Main Methods
    public void startGame() {
        if (FRAME_ON)
            frame = new Frame(this);
        block = newBlock();
        setBlock(block, true, field);
        gameRunning = true;
        if (FRAME_ON)
            frame.repaint();
    }

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            Main main = new Main();
            main.startGame();

            while (main.isGameRunning()) {
                float highestReward = -100000000;
                int action = 0;
                for (int i = 0; i < X_FIELDS * 4; i++) {
                    Main newMain = new Main();
                    newMain.fieldOldBlocks = new boolean[Y_FIELDS][X_FIELDS];
                    int k = 0;
                    for (boolean[] b : main.fieldOldBlocks) {
                        newMain.fieldOldBlocks[k] = b.clone();
                        k++;
                    }
                    newMain.field = new boolean[Y_FIELDS][X_FIELDS];
                    int j = 0;
                    for (boolean[] b : main.field) {
                        newMain.field[j] = b.clone();
                        j++;
                    }
                    newMain.block = new Block();
                    newMain.block.PosX = main.block.PosX.clone();
                    newMain.block.PosY = main.block.PosY.clone();
                    newMain.lastStateReward = main.lastStateReward;

                    float reward = newMain.update(i);
                    if (reward > highestReward) {
                        highestReward = reward;
                        action = i;
                    }
                }
                System.out.println(action);
                TimeUnit.MILLISECONDS.sleep(1000);
                System.out.println("Reward: " + main.update(action));
            }
        }
    }
}
