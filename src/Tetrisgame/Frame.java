package Tetrisgame;

import AI.NeuralNetwork;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Frame extends JFrame implements KeyListener {
    private Main main;
    private final int TOP_BAR_HEIGHT = 26;

    public Frame(Main main){
        this.main = main;
        setTitle("Tetris");
        setSize(main.X_FIELDS*main.FIELD_SIZE, main.Y_FIELDS*main.FIELD_SIZE + TOP_BAR_HEIGHT);
        setVisible(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.setColor(Color.BLACK);
        for (int i = 0; i < main.Y_FIELDS; i++) {
            for (int j = 0; j < main.X_FIELDS; j++) {
                if(i == 0 && j == Main.X_FIELDS - 1) graphics.setColor(NeuralNetwork.exploring ? Color.GREEN : Color.CYAN);
                graphics.fillRect(j * main.FIELD_SIZE, i * main.FIELD_SIZE + TOP_BAR_HEIGHT, main.FIELD_SIZE, main.FIELD_SIZE);
                graphics.setColor(Color.BLACK);
                if (main.field[i][j]) {
                    graphics.setColor(Color.BLUE);
                    graphics.fillRect(j * main.FIELD_SIZE, i * main.FIELD_SIZE + TOP_BAR_HEIGHT, main.FIELD_SIZE, main.FIELD_SIZE);
                    graphics.setColor(Color.BLACK);
                    graphics.drawRect(j * main.FIELD_SIZE, i * main.FIELD_SIZE + TOP_BAR_HEIGHT, main.FIELD_SIZE, main.FIELD_SIZE);
                }
                if (main.fieldOldBlocks[i][j]) {
                    graphics.setColor(Color.RED);
                    graphics.fillRect(j * main.FIELD_SIZE, i * main.FIELD_SIZE + TOP_BAR_HEIGHT, main.FIELD_SIZE, main.FIELD_SIZE);
                    graphics.setColor(Color.BLACK);
                    graphics.drawRect(j * main.FIELD_SIZE, i * main.FIELD_SIZE + TOP_BAR_HEIGHT, main.FIELD_SIZE, main.FIELD_SIZE);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) { }

    public void keyPressed(KeyEvent e) {
        char c = e.getKeyChar();
        if(e.getKeyCode() == 39) c = 'r';
        if(e.getKeyCode() == 37) c = 'l';
        if(e.getKeyCode() == 38) c = 'a';
        if(e.getKeyCode() == 40) c = 'o';

        main.update(c);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) { }
}

