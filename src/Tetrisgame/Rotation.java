package Tetrisgame;

public class Rotation {

    private Main main;
    public int yChange =0;
    public int yMoved = 0;
    public Rotation(Main main) {
        this.main = main;
    }

    public boolean[][] rotate(boolean MatrixOriginal[][], Block block) {
        boolean MatrixNew[][] = new boolean[4][4];
        boolean stop = false;
        for (int i = 0; i < 4; i++) {
            if (stop) {
                break;
            }
            for (int j = 0; j < 4; j++) {
                if (!ValueToHigh(block)) {
                    if (MatrixOriginal[i][j] && main.fieldOldBlocks[block.checkPos2(j + main.arraySorter(block.PosY, true),main.Y_FIELDS,1)]
                            [block.checkPos(3 - i + main.arraySorter(block.PosX, true), main.X_FIELDS,1)]) {
                        MatrixNew = MatrixOriginal;
                        stop = true;
                        break;
                    } else {
                if (MatrixOriginal[i][j])
                    MatrixNew[j][3 - i] = MatrixOriginal[i][j];
                    }
                } else {
                    if(MatrixOriginal[i][j])
                    MatrixNew[j][3 - i] = MatrixOriginal[i][j];
                }
            }
        }
        yChange = main.arraySorter(block.PosY, true);
        boolean whileIsRunning= true;
        while (whileIsRunning){
            yMoved = 0;
            boolean stop1 = false;
            for (int i = 0; i < 4; i++) {
                if(stop1){break;}
                for (int j = 0; j < 4;j++)
                    if(MatrixNew[3-i][j]){
                        while (3-i + yChange - yMoved >=main.Y_FIELDS && !main.fieldOldBlocks[block.checkPos2((3-i + yChange - yMoved),main.Y_FIELDS,1)][j + main.arraySorter(block.PosX, true)]){
                            yMoved++;
                        }
                        whileIsRunning = false;
                        stop1 = true;
                        break;
                    }
            }
        }
        int rightWallAbstand = 2;
        for (int Pos : block.PosX) {
            if (Pos + 1 == Main.X_FIELDS) {
                rightWallAbstand = 0;
                break;
            }
            if (Pos + 2 == Main.X_FIELDS)
                rightWallAbstand = 1;

        }

        if (rightWallAbstand > 0) {
            MatrixNew = checkMatrixVerschiebung(MatrixNew, block, yMoved);
        }
        if (rightWallAbstand > 1) {
            MatrixNew = checkMatrixVerschiebung(MatrixNew, block, yMoved);
        }
        return MatrixNew;
    }

    public boolean[][] checkMatrixVerschiebung(boolean[][] MatrixNew, Block block, int YMoved) {
        boolean MatrixNewer[][] = new boolean[4][4];
        boolean stop = false;
        for (int PosY : block.PosY) {
            if (stop) {
                break;
            }
            for (int PosX : block.PosX) {
                if (!(MatrixNew[0][0] || MatrixNew[1][0] || MatrixNew[2][0] || MatrixNew[3][0]) && !main.fieldOldBlocks[block.checkPos2(PosY + main.arraySorter(block.PosY, true) - YMoved, main.Y_FIELDS,1)][block.checkPos(PosX - 1, -1, (-1))]) {
                    for (int i = 0; i < 4; i++) {
                        for (int j = 0; j < 3; j++) {
                            MatrixNewer[i][j] = MatrixNew[i][j + 1];
                        }
                    }
                    return MatrixNewer;
                } else {
                    MatrixNewer = MatrixNew;
                    stop = true;
                    break;
                }
            }
        }
        return MatrixNew;
    }

    public boolean ValueToHigh(Block block) {
        boolean check = false;
        for (int i = 0; i < 4; i++) {
            if (3 - i + main.arraySorter(block.PosX, true) >= main.X_FIELDS) {
                check = true;
                break;
            }
        }
        return check;
    }

}

