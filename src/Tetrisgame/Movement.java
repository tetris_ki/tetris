package Tetrisgame;

public class Movement {
	
	private final Main main;
	
	public Movement(Main main) {
		this.main = main;
	}

    //Movement options
    public boolean moveDown(Block block) {
        boolean notMovable = checkUnder(block, block.PosY, main.Y_FIELDS, main.Y_FIELDS - 1,
                1, 0,main.X_FIELDS-1,main.Y_FIELDS-1);
        if (notMovable) {
            stopBlock(block);
        } else {
        	main.setBlock(block, false, main.field);
            block.raisePosZ(block);
        }
        return !notMovable;
    }

    public void moveRight(Block block) {  
    	
        if(main.moveCounter % 2 == 1) {
        	//moveDown(block);
        }
        
        boolean notMovable = checkUnder(block, block.PosX, main.X_FIELDS, main.X_FIELDS - 1,
                0, 1 ,main.X_FIELDS-1,main.Y_FIELDS-1);
        if (notMovable) {
            checkDownMove(block);
            return;
        } else {
        	main.setBlock(block, false, main.field);
            block.raisePosB(block, 1);
        }
        main.setBlock(block, true, main.field);
        
        checkDownMove(block);
    }

    public void moveLeft(Block block) {
    	
        if(main.moveCounter % 2 == 1) {
        	//moveDown(block);
        }
        
        boolean notMovable = checkUnder(block, block.PosX, -1, 0, 0, -1,-1,-1);
        if (notMovable) {
            checkDownMove(block);
            return;
        }

        main.setBlock(block, false, main.field);
        block.raisePosB(block, -1);
        main.setBlock(block, true, main.field);
        
        checkDownMove(block);
    }

    //Movement calculation
    public boolean checkUnder(Block block, int[] Pos, int IndexOut, int IndexMax,
                              int YChange, int XChange, int IndexOutOldX,int IndexOutOldY) {
	    boolean isBlocked = false;
	    for (int singlePos: Pos) {
            if(block.checkPos(singlePos, IndexOut, XChange) == IndexMax){
                isBlocked = true;
                break;
            }
        }
        if (!isBlocked) {
            for (int i = 0; i < block.PosY.length; i++) {
                if (main.fieldOldBlocks[block.PosY[i] + YChange][block.PosX[i] + XChange]) {
                    isBlocked = true;
                    break;
                }
            }
        }
	    return isBlocked;
    }
    
    public void checkDownMove(Block block) {
        
        if(main.moveCounter % 2 == 0) {
        	boolean nnotMovable = checkUnder(block, block.PosY, main.Y_FIELDS, main.Y_FIELDS - 1,
                    1, 0,main.X_FIELDS-1,main.Y_FIELDS-1);
        	if (nnotMovable)
                stopBlock(block);
            
        }
        
        main.moveCounter++;
    }

    public void stopBlock(Block a) {
        main.setBlock(a, true, main.fieldOldBlocks);
        main.setBlock(a, false, main.field);
        main.checkLine(a);
        main.block = main.newBlock();
    }

}
